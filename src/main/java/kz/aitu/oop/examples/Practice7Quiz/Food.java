package kz.aitu.oop.examples.Practice7Quiz;

public interface Food {
    public String getType();

}
class Cake implements Food{
    public String getType() {
        return "Someone ordered a Dessert!";
    }
}
class Pizza implements Food{
    public String getType() {
        return "Someone ordered a Fast Food!";
    }
}
class FoodFactory {

    public FoodFactory(String type){
        System.out.println("The factory returned " + getFood(type).getClass());
        System.out.println(getFood(type).getType());
    }

    public Food getFood(String type){
        type = type.toLowerCase();
        if (type.equals("cake")){
            return new Cake();
        }else if(type.equals("pizza")){
            return new Pizza();
        }else {
            return null;
        }
    }

}
