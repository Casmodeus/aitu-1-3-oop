package kz.aitu.oop.examples;

import java.util.ArrayList;


public class Shape {
    private ArrayList figure;
    private int index;
    private int[] sides;
    public Shape(){
        figure = null;
        index = 0;
    }
    public void addPoint(int x,int y){
        Point point_to_add = new Point(x,y);
        figure.add(index,point_to_add);
        //SORRY FOR UNREADABLE CODE ('O_O);
        if(index >= 1){
            sides[index-1] = (int) Math.sqrt(Math.pow((((Point)figure.get(index)).getPoint()[0] - ((Point)figure.get(index-1)).getPoint()[0]),2) + Math.pow((((Point)figure.get(index)).getPoint()[1] - ((Point)figure.get(index-1)).getPoint()[1]),2));
        }
        index++;
    }
    public ArrayList getPoints(){
        return figure;
    }
    public int calculatePerimeter(){
        int size = sides.length;
        int perimeter = 0;
        for(int a = 0; a < size ; a++){
            perimeter += sides[a];
        }
        return perimeter;
    }
    public int longestside(){
        int longest = 0;
        for(int a = 0; a < sides.length ; a++){
            if (sides[a] > longest){
                longest = sides[a];
            }
        }
        return longest;
    }
    public int averagelength(){
        return calculatePerimeter()/sides.length;
    }
}
