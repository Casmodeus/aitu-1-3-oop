package kz.aitu.oop.examples.Practice6Quiz;

public class Singleton {
    public String str;

    //private non-parameterized constructor
    private Singleton(){

    }

    public static Singleton instance = new Singleton();

    public static Singleton getSingleInstance(){
        return instance;
    }

}
