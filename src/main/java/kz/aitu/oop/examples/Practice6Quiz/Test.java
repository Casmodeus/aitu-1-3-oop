package kz.aitu.oop.examples.Practice6Quiz;


import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        String str2 = sc1.nextLine();
        Singleton sing1 = Singleton.getSingleInstance();
        sing1.str = "Hello i am singleton! Let me say " + str2 + " to you";
        System.out.println(sing1.str);
    }
}
