package kz.aitu.oop.examples.Assignment5;


import lombok.Data;



@Data
public class Shape {
    private String color;
    private boolean filled;

    public Shape(){
        this.color = "green";
        this.filled = true;
    }

    public Shape(String color,boolean filled){
        this.color = color;
        this.filled = filled;
    }

    public String toString(){
        String result = "";
        if (this.filled) {
            result = "A shape with color of " + this.color + " and filled";
        }else {
            result = "A shape with color of " + this.color + " and not filled";
        }
        return result;
    }


}
