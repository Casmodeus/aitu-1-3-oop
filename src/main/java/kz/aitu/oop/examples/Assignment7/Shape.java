package kz.aitu.oop.examples.Assignment7;

import lombok.Data;

@Data

public abstract class Shape {
    private String color;
    private boolean filled;

    public Shape(){
        this.color = "red";
        this.filled = true;
    }

    public Shape(String color,boolean filled){
        this.color = color;
        this.filled = filled;
    }

    public String toString(){
        String result = "";
        if (this.filled) {
            result = "A shape with color of " + this.color + " and filled";
        }else {
            result = "A shape with color of " + this.color + " and not filled";
        }
        return result;
    }
    public abstract double getArea();
    public abstract double getPerimeter();
}
