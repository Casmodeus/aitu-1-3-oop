package kz.aitu.oop.examples.Assignment7;

import lombok.Data;

@Data
public class Square extends Rectangle {

    public Square(){
        new Rectangle();
    }
    public Square(double side){
        this.setWidth(side);
        this.setLength(side);
    }
    public Square(double side, String color, boolean filled){
        super(side,side,color,filled);

    }
    //No,we dont need to override methods getArea() and getPerimeter()

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
    }

    public String toString(){
        return "A Square with side=" + this.getWidth() +", which is a subclass of " + super.toString();
    }
}
