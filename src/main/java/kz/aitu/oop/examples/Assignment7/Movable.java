package kz.aitu.oop.examples.Assignment7;

public interface Movable {
    public void moveUp();
    public void moveDown();
    public void moveLeft();
    public void moveRight();

}
