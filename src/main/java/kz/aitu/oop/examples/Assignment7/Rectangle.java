package kz.aitu.oop.examples.Assignment7;

import lombok.Data;
@Data


public class Rectangle extends Shape {
    private double width;
    private double length;

    public Rectangle(){
        this.width = 1.0;
        this.length = 1.0;
    }
    public Rectangle(double width, double length){
        this.width = width;
        this.length = length;
    }
    public Rectangle(double width, double length, String color, boolean filled){
        super(color,filled);
        this.width = width;
        this.length = length;
    }

    public double getArea(){
        return width*length;
    }
    public double getPerimeter(){
        return 2*(width+length);
    }
    public String toString(){
        return "A Rectangle with width="+ this.getWidth() + "and length="+ this.getLength() + ", which is a subclass of " + super.toString();
    }
}
