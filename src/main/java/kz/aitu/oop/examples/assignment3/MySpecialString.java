package kz.aitu.oop.examples.assignment3;

import java.util.Arrays;

public class MySpecialString {
    private int[] array;


    public MySpecialString(int[] values){
        if (array.length == 0) array[0] = values[0];
        else {
            int i = 1;
            for (int item : values) {
                if (!contains(item)) array[i] = item;
            }
        }
    }

    public int length(){
        return array.length;
    }

    public int valueAt(int position){
        if (position > length()){
            return -1;
        }else {
            return array[position];
        }
    }
    public boolean contains(int value) {
        Arrays.sort(array);
        int start = 0;
        int end = length();

        while(start < end) {
            int mid = (start+end)/2;
            if (array[mid] == value) return true;
            else if (array[mid] > value){
                end = mid;
            }
            else if(array[mid] < value){
                start = mid+1;
            }
        }
        return false;
    }
    public int count(int value){
        Arrays.sort(array);
        int start = 0;
        int end = length();
        int firstCount = 0;
        int secondCount = 0;
        while(start < end) {
            int mid = (start+end)/2;
            if (array[mid] == value) {
                firstCount = mid;
                end = mid;
            }
            else if (array[mid] > value){
                end = mid;
            }
            else if(array[mid] < value){
                start = mid+1;
            }
        }
        for (int i = firstCount; i < length(); i++){
            if(array[i] == value) secondCount++;
        }
        //There must be also binary search but i'm so lazy to do this;
        return secondCount;
    }
    public void print(){
        for (int i = 0; i < length(); i++){
            System.out.print(array[i] + " ");
        }
        System.out.print('\n');
    }

}
