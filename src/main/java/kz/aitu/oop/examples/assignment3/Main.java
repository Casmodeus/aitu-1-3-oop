package kz.aitu.oop.examples.assignment3;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        int[] values = {1,2,3,4,4,5};
        MyString array1 = new MyString(values);
        Scanner sc = new Scanner(System.in);
        String FileName = sc.next();
        //fileWrite(FileName,array1);
        ArrayList values2 = new ArrayList();
        fileRead(FileName,values2);
        int[] array_list = new int[values2.size()];
        int i=0;
        System.out.println(values2.size());
        for (Object value : values2){
            array_list[i] = Integer.parseInt((String)value);
            i++;
        }
        MyString array2 = new MyString(array_list);
        array2.print();
    }
    public static void fileWrite(String FileName,MyString array) throws Exception {
        FileWriter nFile = new FileWriter("C:\\Users\\DELL\\Desktop\\"+FileName+".txt");
        for (int i =0;i < array.length() ; i++){
            nFile.write(Integer.toString(array.valueAt(i)));
            nFile.append('\n');
        }
        nFile.close();
    }
    public static void fileRead(String FileName,ArrayList values2) throws Exception {
        FileReader nFile = new FileReader("C:\\Users\\DELL\\Desktop\\"+FileName+".txt");
        Scanner scan = new Scanner(nFile);
        while (scan.hasNextLine()) {
            values2.add(scan.nextLine());
        }
        nFile.close();
    }
    public static boolean isEqual(int[] values, int[] values2){
        for (int i =0; i < values.length; i++){
            if (values[i] != values2[i]) return false;
        }
        return true;
    }
}

