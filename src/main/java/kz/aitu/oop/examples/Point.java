package kz.aitu.oop.examples;

public class Point{
    private int x;
    private int y;
    public Point(int x,int y){
        this.x = x;
        this.y = y;

    }
    public int[] getPoint(){
        return new int[]{this.x,this.y};
    }
    public int dist(int x1,int y1){
        int[] point2 = (new Point(x1,y1)).getPoint();
        return (int) Math.sqrt(Math.pow((point2[0] - this.x),2) + Math.pow((point2[1]-this.y),2));
    }
}
