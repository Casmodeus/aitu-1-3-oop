package kz.aitu.oop.examples.Assignment7SubTask3;

public class ResizableCircle extends Circle implements Resizable {
    public ResizableCircle(double radius){
        super(radius);
    }
    public ResizableCircle(){
        super();
    }
    @Override
    public double resize(int percent) {
        double rad = super.getRadius() * (1 + ((double)percent/100));
        super.setRadius(rad);
        return rad;
    }
}
