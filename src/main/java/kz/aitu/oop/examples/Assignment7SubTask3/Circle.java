package kz.aitu.oop.examples.Assignment7SubTask3;

import lombok.Data;
import lombok.ToString;
@Data
@ToString
public class Circle implements GeometricObject{
    private double radius;

    public Circle(){
        this.radius = 1.0;
    }
    public Circle(double radius){
        this.radius = radius;
    }
    @Override
    public double getPerimeter() {
        return Math.PI * (2 * radius);
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }
}
