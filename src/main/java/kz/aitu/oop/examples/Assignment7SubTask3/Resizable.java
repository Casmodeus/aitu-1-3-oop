package kz.aitu.oop.examples.Assignment7SubTask3;

public interface Resizable {
    public double resize(int percent);
}
