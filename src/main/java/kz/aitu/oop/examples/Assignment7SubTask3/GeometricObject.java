package kz.aitu.oop.examples.Assignment7SubTask3;

public interface GeometricObject {
    public double getPerimeter();
    public double getArea();
}