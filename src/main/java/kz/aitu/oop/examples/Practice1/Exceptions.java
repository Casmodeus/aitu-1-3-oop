package kz.aitu.oop.examples.Practice1;

public class Exceptions {
    public static void main(String[] args){
        try {
            throw new Exception("There a String variable");
        }catch (Exception exception){
            System.out.println(exception.getMessage());
        } finally {
            System.out.println("I've been here");
        }
    }
}
