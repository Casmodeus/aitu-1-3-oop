package kz.aitu.oop.examples.Practice1;



import java.io.File;
import java.io.FileNotFoundException;



public class FileSearch {
    //null constructor

    public static boolean fileExists(String Filename){
        File dir = new File("name");
        String[] files = dir.list();
        try {
            for (int i = 0;i < files.length; i ++){
                if (files[i] == Filename) return true;
            }
            throw new FileNotFoundException("File Not Found");
        }catch (FileNotFoundException e){
            System.out.println(e.getMessage());
        }finally {
            try {
                if (Filename == ""){
                    throw new EmptyStringException("FileName cannot be empty!");
                }
            }catch (EmptyStringException e){
                System.out.println(e.getMessage());
            }
            finally {
                return false;
            }
        }
    }
    public static boolean isInt(String S) throws NumberFormatException{
        boolean check = true;
        try {
            Integer newInt = Integer.parseInt(S);
        }catch (NullPointerException | NumberFormatException e ){
            System.out.println(e.getMessage());
            check = false;
        }finally {
            return check;
        }
    }
    public static boolean isDouble(String S) throws NumberFormatException {
        boolean check = true;
        try {
            Double newInt = Double.parseDouble(S);
        }catch (NullPointerException | NumberFormatException e ){
            System.out.println(e.getMessage());
            check = false;
        }finally {
            return check;
        }
    }
}

