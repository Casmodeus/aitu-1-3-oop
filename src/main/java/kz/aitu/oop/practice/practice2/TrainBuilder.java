package kz.aitu.oop.practice.practice2;

public class TrainBuilder implements Builder{
    private  Type type;
    private int speed;
    private int travelDistanceCapacity;
    private  String trainName;


    @Override
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public void setTravelDistanceCapacity(int travelDistanceCapacity) {
        this.travelDistanceCapacity = travelDistanceCapacity;
    }

    @Override
    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }

    @Override
    public void setType(Type type) {
        this.type = type;
    }

    public Train create(){
        if (this.type.equals(Type.HS_rail)){
            return new RailwayCarriage(Type.HS_rail,1000,0,this.speed,this.travelDistanceCapacity,this.trainName);
        }else return new Locomotive(this.type,10000,this.speed,this.travelDistanceCapacity,this.trainName);
    }
}
