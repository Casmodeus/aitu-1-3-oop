package kz.aitu.oop.practice.practice2;

public interface Builder {
    void setSpeed(int speed);
    void setTravelDistanceCapacity(int travelDistanceCapacity);
    void setTrainName(String trainName);
    void setType(Type type);
}
