package kz.aitu.oop.practice.practice2;

public class Test {
    public static void main(String[] args) {
        Director director = new Director();
        TrainBuilder trainBuilder = new TrainBuilder();
        director.constructHS_Rail(trainBuilder);
        Train train1 = trainBuilder.create();
        System.out.println(train1.toString());
        TrainBuilder trainBuilder1 = new TrainBuilder();
        director.constructLocomotive(trainBuilder1);
        Train train2 = trainBuilder1.create();
        System.out.println(train2);
    }
}
