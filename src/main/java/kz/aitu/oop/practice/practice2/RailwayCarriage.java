package kz.aitu.oop.practice.practice2;

public class RailwayCarriage extends Train{
    private final Type type;
    private final int seats;
    private int passengers;
    private int speed;
    private int travelDistanceCapacity;
    private final String trainName;

    public RailwayCarriage(Type type, int seats, int passengers, int speed, int travelDistanceCapacity, String trainName) {
        this.type = type;
        this.seats = seats;
        this.passengers = passengers;
        this.speed = speed;
        this.travelDistanceCapacity = travelDistanceCapacity;
        this.trainName = trainName;

        //Regional trains cannot run with speed more than 200 km/h
        if (type.equals(Type.Regional) && speed > 200) {
            //setup to the maximum speed
            this.setSpeed(200);
        }else if (type.equals(Type.HS_rail) &&  speed > 500){
            //maximum for HS_rail
            this.setSpeed(450);
        }
    }

    public int getAvailableSeats(){
        return seats-passengers;
    }


    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setTravelDistanceCapacity(int travelDistanceCapacity) {
        this.travelDistanceCapacity = travelDistanceCapacity;
    }

    @Override
    public String toString() {
        return "A train " + this.trainName + " with speed " + this.speed + " and " + this.seats + " seats";
    }
}
