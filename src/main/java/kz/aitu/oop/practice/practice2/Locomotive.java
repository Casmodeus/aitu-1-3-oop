package kz.aitu.oop.practice.practice2;

public class Locomotive extends Train{
    private final Type type;
    private final int carryingCapacity;
    private int speed;
    private int travelDistanceCapacity;
    private final String trainName;

    public Locomotive(Type type, int carryingCapacity, int speed, int travelDistanceCapacity, String trainName) {
        this.type = type;
        this.carryingCapacity = carryingCapacity;
        this.speed = speed;
        this.travelDistanceCapacity = travelDistanceCapacity;
        this.trainName = trainName;
    }

    @Override
    public String toString() {
        return "A train " + this.trainName + " with speed " + this.speed + " and " + this.carryingCapacity + " carrying capacity";
    }
}
