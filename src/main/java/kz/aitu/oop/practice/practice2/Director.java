package kz.aitu.oop.practice.practice2;

public class Director {
    public static int number = 1;

    public void constructHS_Rail(TrainBuilder trainBuilder){

        trainBuilder.setSpeed(440);
        trainBuilder.setTrainName("HS_Rail №" + number);
        trainBuilder.setTravelDistanceCapacity(1200);
        trainBuilder.setType(Type.HS_rail);
        number += 1;
    }
    public void constructRegional(TrainBuilder trainBuilder){
        trainBuilder.setSpeed(200);
        trainBuilder.setTrainName("Regional №" + number);
        trainBuilder.setTravelDistanceCapacity(2500);
        trainBuilder.setType(Type.Regional);
        number += 1;
    }
    public void constructLocomotive(TrainBuilder trainBuilder){
        trainBuilder.setSpeed(150);
        trainBuilder.setTrainName("Locomotive №" + number);
        trainBuilder.setTravelDistanceCapacity(8000);
        trainBuilder.setType(Type.LOCOMOTIVE);
        number += 1;
    }
}
