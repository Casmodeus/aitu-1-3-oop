package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.repository.StudentFileRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.*;

//import kz.aitu.oop.repository.StudentDBRepository;

@RestController
@RequestMapping("/api/task/4")
@AllArgsConstructor
public class AssignmentController4 {


    /**
     *
     * @param group
     * @return all student name by group name
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}")
    public ResponseEntity<?> getStudentsByGroup(@PathVariable("group") String group) throws FileNotFoundException {

        //write your code here
        String result = "";
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student: studentFileRepository.getStudents()) {
            if (student.getGroup().equals(group)){
                result += student;
                //???NEW LINE DOESN'T WORKING???
                result += "\r\n";
            }
        }


        return ResponseEntity.ok(result);
    }

    /**
     *
     * @param group
     * @return stats by point letter (counting points): example  A-3, B-4, C-1, D-1, F-0
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}/stats")
    public ResponseEntity<?> getGroupStats(@PathVariable("group") String group) throws FileNotFoundException {

        //write your code here
        String result = "";
        ArrayList A = new ArrayList();
        ArrayList B = new ArrayList();
        ArrayList C = new ArrayList();
        ArrayList D = new ArrayList();
        ArrayList F = new ArrayList();
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student: studentFileRepository.getStudents()) {
            if (student.getGroup().equals(group)){
                double points = student.getPoint();
                if (points >= 90) A.add(student.getName());
                else if (points >= 80 && points < 90) B.add(student.getName());
                else if(points >= 70 && points < 80) C.add(student.getName());
                else if (points >= 59 && points < 70) D.add(student.getName());
                else F.add(student.getName());
            }
        }
        result += "A-" + A.size() + " B-" + B.size() + " C-" + C.size() + " D-" +  D.size() + " F-" + F.size();
        return ResponseEntity.ok(result);
    }

    /**
     *
     * @return top 5 students name by point
     * @throws FileNotFoundException
     */
    @GetMapping("/students/top")
    public ResponseEntity<?> getTopStudents() throws FileNotFoundException {

        //write your code here
        String result = "";
        LinkedHashMap<String,Double> studPoints = new LinkedHashMap<String, Double>();
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student: studentFileRepository.getStudents()) {
            studPoints.put(student.getName(),student.getPoint());
        }
        List<Map.Entry<String,Double>> entries = new ArrayList<Map.Entry<String, Double>>(studPoints.entrySet());
        Collections.sort(entries, new Comparator<Map.Entry<String, Double>>() {
            @Override
            public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
                return (int) (o2.getValue() - o1.getValue());
            }
        });
        studPoints.clear();
        for (Map.Entry<String,Double> entry: entries){
            studPoints.put(entry.getKey(),entry.getValue());
        }
        result += studPoints.toString();
        return ResponseEntity.ok(result);
    }
}
