package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.examples.Point;
import kz.aitu.oop.repository.StudentFileRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/task/2")
public class AssignmentController2 {


    @GetMapping("/students")
    public ResponseEntity<?> getStudents() throws FileNotFoundException {

        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String result = "";
        for (Student student: studentFileRepository.getStudents()) {
            result += student.getName() + "\t" + student.getAge() + "\t" + student.getPoint() + "</br>";
        }

        return ResponseEntity.ok(result);
    }

    /**
     * Method get all Students from file and calculate average name lengths
     * @return average name length of the all students
     * @throws FileNotFoundException
     */
    @GetMapping("/averageStudentsNameLength")
    public ResponseEntity<?> averageStudentsNameLength() throws FileNotFoundException {

        double average = 0;
        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        int count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total += student.getName().length();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }


    @GetMapping("/averageStudentsCount")
    public ResponseEntity<?> averageStudentsCount() throws FileNotFoundException {

        double count = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        ArrayList groups = new ArrayList();


        for (Student student: studentFileRepository.getStudents()) {
            if(!groups.contains(student.getGroup())){
                groups.add(student.getGroup());
            }
            count++;
        }
        double answer = count/ groups.size();
        return ResponseEntity.ok(answer);
    }

    @GetMapping("/averageStudentsPoint")
    public ResponseEntity<?> averageStudentsPoint() throws FileNotFoundException {
        int count = 0;
        double average = 0;
        double total_point = 0;
        //change your code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student: studentFileRepository.getStudents()) {
            total_point += student.getPoint();
            count ++;
        }
        average = total_point/count;
        return ResponseEntity.ok(average);
    }

    @GetMapping("/averageStudentsAge")
    public ResponseEntity<?> averageStudentsAge() throws FileNotFoundException {

        double average = 0;
        int count = 0;
        double total_age = 0;
        //change your code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student: studentFileRepository.getStudents()) {
            total_age += student.getAge();
            count ++;
        }
        average = total_age/count;
        return ResponseEntity.ok(average);
    }

    @GetMapping("/highestStudentsPoint")
    public ResponseEntity<?> highestStudentsPoint() throws FileNotFoundException {

        double maxPoint = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student: studentFileRepository.getStudents()) {
            if (student.getPoint() > maxPoint){
                maxPoint = student.getPoint();
            }
        }

        return ResponseEntity.ok(maxPoint);
    }

    @GetMapping("/highestStudentsAge")
    public ResponseEntity<?> highestStudentsAge() throws FileNotFoundException {

        double maxAge = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student: studentFileRepository.getStudents()) {
            if (student.getAge() > maxAge){
                maxAge = student.getAge();
            }
        }

        return ResponseEntity.ok(maxAge);
    }

    @GetMapping("/highestGroupAveragePoint")
    public ResponseEntity<?> highestGroupAveragePoint() throws FileNotFoundException {

        double averageGroupPoint = 0;
        double total_group_points = 0;
        int count = 0;
        String group = "";
        ArrayList group_points = new ArrayList();
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student: studentFileRepository.getStudents()) {
            if (group.equals("")) group = student.getGroup();
            if (group.equals(student.getGroup())) {
                total_group_points += student.getPoint();
                count++;

            }else {
                group_points.add(total_group_points/count);
                group = student.getGroup();
                total_group_points = 0;
                count=0;
                total_group_points += student.getPoint();
                count++;
            }

        }
        group_points.add(total_group_points/count);
        for(int a = 0; a < group_points.size(); a ++){
            if ((double)group_points.get(a) > averageGroupPoint){
                averageGroupPoint = (double)group_points.get(a);
            }
        }
        return ResponseEntity.ok(averageGroupPoint);
    }

    @GetMapping("/highestGroupAverageAge")
    public ResponseEntity<?> highestGroupAverageAge() throws FileNotFoundException {

        double averageGroupAge = 0;
        double total_group_age = 0;
        int count = 0;
        String group = "";
        ArrayList group_age = new ArrayList();
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student: studentFileRepository.getStudents()) {
            if (group.equals("")) group = student.getGroup();
            if (group.equals(student.getGroup())) {
                total_group_age += student.getAge();
                count++;

            }else {
                group_age.add(total_group_age/count);
                group = student.getGroup();
                total_group_age = 0;
                count=0;
                total_group_age += student.getAge();
                count++;
            }

        }
        group_age.add(total_group_age/count);
        for(int a = 0; a < group_age.size(); a ++){
            if ((double)group_age.get(a) > averageGroupAge){
                averageGroupAge = (double)group_age.get(a);
            }
        }


        return ResponseEntity.ok(averageGroupAge);
    }


    @GetMapping("/all")
    public ResponseEntity<?> allData() throws FileNotFoundException {

        String result = "";
        result += "averageStudentsNameLength: " + averageStudentsNameLength().getBody() + "</br>";
        result += "averageStudentsCount: " + averageStudentsCount().getBody() + "</br>";
        result += "averageStudentsPoint: " + averageStudentsPoint().getBody() + "</br>";
        result += "averageStudentsAge: " + averageStudentsAge().getBody() + "</br>";
        result += "highestStudentsPoint: " + highestStudentsPoint().getBody() + "</br>";
        result += "highestStudentsAge: " + highestStudentsAge().getBody() + "</br>";
        result += "highestGroupAveragePoint: " + highestGroupAveragePoint().getBody() + "</br>";
        result += "highestGroupAverageAge: " + highestGroupAverageAge().getBody() + "</br>";

        return ResponseEntity.ok(result);
    }
}
